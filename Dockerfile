FROM alpine:3.6
MAINTAINER c0ldcat <c0ldcat3z@gmail.com>

# Install python and pip
RUN apk add --no-cache python3 py-pip

RUN apk add --no-cache ca-certificates

RUN pip3 install setuptools --upgrade

COPY requirements.txt /src/
COPY setup.py /src/
COPY basiccalibre /src/basiccalibre

RUN cd /src && \
    pip3 install -r requirements.txt && \
    python3 setup.py install

ENV HOST 0.0.0.0
ENV PORT 80
ENV DATABASE .

CMD basiccalibre -l ${HOST} -p ${PORT} -d ${DATABASE}
