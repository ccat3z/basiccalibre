# Basic Calibre Server

Basic REST calibre api server based on calibre database

## How to use

`python -m basiccalibre -l HOST -p PORT -d DATABASE`

## Support database storage

### Local

`/path/to/database`

### HTTP File Server

`http://example.com/path/to/database`

### Microsoft SharePoint

`sp://{BASE64_CODE}`

#### Encode SharePoint url

create pass.json:

```
{
    "path": "path/to/database",
    "sharepoint": "site prefix, e.g: example for example-my.sharepoint.com",
    "user-agent": "user-agent of login browser",
    "cookie": "cookie of site"
}
```

encode pass.json on linux:

```
base64 -w0 pass.json
```
