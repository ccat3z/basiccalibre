#! /usr/bin/python3

from flask import Flask
from flask_restful import Resource, Api, abort

import re
import logging
from logging.config import dictConfig
import argparse
import json, base64, binascii
from urllib.error import HTTPError

from .resources.book import AllBook, Book
from .resources.tag import AllTag
from .common.pycalibre import CalibreDB
from .common.filebase import FileBase, HttpFileBase, SharePointFileBase

DB_PATH = 'metadata.db'

def run(host, port, database):
    logger = logging.getLogger()

    if re.match('^https{0,1}://', database):
        fb = HttpFileBase(database)
    elif re.match('^sp://', database):
        try:
            config = json.loads(
                         base64.b64decode(re.sub('^sp://', '',
                                          database).encode('UTF-8')))
            fb = SharePointFileBase(config['path'], config['sharepoint'],
                                    config['user-agent'], config['cookie'])
        except binascii.Error:
            logger.error('incorrect sharepoint database base64 code')
            exit(1)
        except KeyError as e:
            logger.error('miss key ' + str(e))
            exit(1)
    else:
        fb = FileBase(database)

    # init flask
    app = Flask(__name__)
    api = Api(app)

    db = CalibreDB()

    app.config['ERROR_404_HELP'] = False
    api.add_resource(AllBook, '/', '/book', '/book/', '/book/<path:root>',
                     resource_class_kwargs = {'db': db, 'fb': fb,
                                              'db_path': DB_PATH})
    api.add_resource(Book, '/book/<int:book_id>',
                     resource_class_kwargs = {'db': db, 'fb': fb})
    api.add_resource(AllTag, '/tag', '/tag/', '/tag/<path:root>',
                     resource_class_kwargs = {'db': db})

    logger.info('fetching database...')

    try:
        AllBook(db, fb, DB_PATH).patch()
    except HTTPError as e:
        logger.error(str(e))
        exit(1)

    # run server
    app.run(host, port)

    # clean temp files
    fb.clean()

def start():
    # set up logger
    logging_config = dict(
        version = 1,
        formatters = {
            'f': {
                'format': '%(asctime)s [%(levelname)s] %(message)s'
            }
        },
        handlers = {
            'h': {
                'class': 'logging.StreamHandler',
                'formatter': 'f',
                'level': logging.INFO
            }
        },
        root = {
            'handlers': ['h'],
            'level': logging.DEBUG
        })
    dictConfig(logging_config)
    
    # parse arguments
    parser = argparse.ArgumentParser(prog='basiccalibre',
                                     description='CalibreDB Server')
    parser.add_argument('-l', '--host', help = 'hostname', default = 'localhost')
    parser.add_argument('-p', '--port', help = 'port', default = '5000')
    parser.add_argument('-d', '--database', help = 'calibre database location',
                        required = True)
    
    args = parser.parse_args()
    
    run(**args.__dict__)
