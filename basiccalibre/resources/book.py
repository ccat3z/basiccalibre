#! /usr/bin/python3

from flask_restful import Resource, abort, reqparse
from ..common.pycalibre import CalibreDB
import re

class AllBook(Resource):
    def __init__(self, db, fb, db_path):
        self.db = db
        self.fb = fb
        self.db_path = db_path

    def _get(self, tag = 'ANY'):
        return {'books': self.db.all_book_id(tag)}

    def get(self, root = ''):
        parser = reqparse.RequestParser()
        parser.add_argument('expand', type = bool, default = False)

        args = parser.parse_args()

        root = re.sub('\.$', '', root.replace('/', '.'))
        resp = self._get(root)

        if args['expand']:
            resp['books'] = dict(map(lambda x:
                                         (x, Book(self.db, self.fb).get(x)),
                                     resp['books']))

        resp['sub-tags'] = list(self.db.all_tag(root))
        return resp

    def patch(self):
        self.db.set_db(self.fb.get(self.db_path))
        return self._get()

class Book(Resource):
    def __init__(self, db, fb):
        self.db = db
        self.fb = fb

    def get(self, book_id):
        parser = reqparse.RequestParser()
        parser.add_argument('url', type = bool, default = False)

        args = parser.parse_args()

        try:
            book_info = self.db.get_book_info(book_id)
            if args['url']:
                book_info['format'] = dict(map(
                            lambda x:
                                (x.split('.')[-1],
                                    self.fb.url(book_info['path'] + '/' + x)),
                            book_info['format']))
            del book_info['path']
            return book_info
        except IndexError:
            abort(404, messages = 'No such book {}'.format(book_id))

