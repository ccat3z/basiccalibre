#! /usr/bin/python3

from flask_restful import Resource, abort, reqparse
import re

class AllTag(Resource):
    def __init__(self, db):
        self.db = db

    def get(self, root = ''):
        return {'tags': list(self.db.all_tag(
                                 re.sub('\.', '', root.replace('/', '.'))))}
