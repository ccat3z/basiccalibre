#! /usr/bin/python3

import sqlite3, re, copy

class CalibreDB:
    """Calibre api"""

    SQL_COMMAND_ALL_BOOKS = "SELECT id FROM books ORDER BY id"
    SQL_COMMAND_GET_BOOKS_INFO = '''SELECT
books.title, authors.name, books.path
FROM books
INNER JOIN books_authors_link AS b_authors
INNER JOIN authors
WHERE books.id = b_authors.book
AND b_authors.author = authors.id 
AND books.id = {}
'''
    SQL_COMMAND_GET_BOOK_FORMAT = '''SELECT name || '.' || lower(format)
FROM data WHERE book = {}'''

    SQL_COMMAND_GET_BOOK_TAG = '''SELECT tags.name FROM books
INNER JOIN books_tags_link AS b_tags INNER JOIN tags
ON books.id = b_tags.book
AND b_tags.tag = tags.id AND books.id = {}'''
    
    SQL_COMMAND_ALL_TAGS = 'SELECT name FROM tags'
    SQL_COMMAND_ALL_BOOKS_FROM_TAGS = '''SELECT books.id FROM books
INNER JOIN books_tags_link AS b_tags
INNER JOIN tags
ON books.id = b_tags.book
AND b_tags.tag = tags.id
AND tags.name = "{}"'''

    EXECUTE_FUNCTOR_DICT = lambda x: dict(x)
    EXECUTE_FUNCTOR_SINGLE_LIST = lambda x: list(x)[0]
    
    def __init__(self, db_path = '/tmp/data.db'):
        self.set_db(db_path)
        self.cache = {}

    def set_db(self, db_path):
        """Set metadata.db"""
        self._db_con = sqlite3.connect(db_path)
        self._db_con.row_factory = sqlite3.Row
        self._db_cur = self._db_con.cursor()
        self.cache={}

    def _execute(self, command, f = EXECUTE_FUNCTOR_DICT):
        """Execute SQL command
        
        Parameters
        ----------
        command
            sqlite3 command
        f
            function for handling each row

        Return
        ------
        list of result
        """

        try:
            reuslt = self.cache[command]
        except KeyError:
            self._db_cur.execute(command)
            reuslt = self._db_cur.fetchall()
            self.cache[command] = reuslt

        resp = list(map(f, reuslt))
        return resp

    def all_book_id(self, tag = 'ANY'):
        """Get all book id under tag"""

        if tag == 'ANY':
            return self._execute(CalibreDB.SQL_COMMAND_ALL_BOOKS,
                                 CalibreDB.EXECUTE_FUNCTOR_SINGLE_LIST)
        else:
            return self._execute(
                CalibreDB.SQL_COMMAND_ALL_BOOKS_FROM_TAGS.format(tag),
                CalibreDB.EXECUTE_FUNCTOR_SINGLE_LIST)

    def get_book_info(self, book_id):
        """Get book info"""

        try:
            info = self._execute(
                    CalibreDB.SQL_COMMAND_GET_BOOKS_INFO.format(book_id))[0]
            info["tag"] = self._execute(
                    CalibreDB.SQL_COMMAND_GET_BOOK_TAG.format(book_id),
                    CalibreDB.EXECUTE_FUNCTOR_SINGLE_LIST)
            info["format"] = self._execute(
                    CalibreDB.SQL_COMMAND_GET_BOOK_FORMAT.format(book_id),
                    CalibreDB.EXECUTE_FUNCTOR_SINGLE_LIST)
        except IndexError:
            raise IndexError("No such book")

        return info

    def all_tag(self, root_tag = ''):
        """Get all tag under tag"""

        root_tag = re.sub('^\.', '', root_tag + '.')
        return set(map(lambda x: x.split('.')[root_tag.count('.')],
                       filter(lambda x: x.startswith(root_tag),
                              self._execute(
                                  CalibreDB.SQL_COMMAND_ALL_TAGS,
                                  CalibreDB.EXECUTE_FUNCTOR_SINGLE_LIST))))

if __name__ == '__main__':
    db = CalibreDB()
