#! /usr/bin/python3

import urllib.request, shutil, tempfile
from urllib.parse import quote
import re, json

class FileBase:
    """Local File Base"""

    def __init__(self, pwd):
        self.pwd = re.sub('/$', '', pwd)

    def get(self, path):
        """Get file path on local storage"""

        return self.pwd + '/' + path

    def url(self, path):
        """Return relative file url"""

        return self.pwd + '/' + path

    def clean(self):
        pass

class HttpFileBase(FileBase):
    """Http File Base

    Notes
    -----
    Remeber call clean() to delete caches when file base is useless
    """

    def __init__(self, pwd):
        super().__init__(pwd)
        self.dtemp = tempfile.mkdtemp()

    def get(self, path):
        """Get file path on local cache"""

        cache = tempfile.mkstemp(dir=self.dtemp)[1]
        with open(cache, 'wb') as cache_file:
            f = urllib.request.urlopen(self.url(path))
            shutil.copyfileobj(f, cache_file)
        
        return cache

    def clean(self):
        """Clean local cache"""

        shutil.rmtree(self.dtemp)

class SharePointFileBase(HttpFileBase):
    """SharePoint File Base"""

    SHAREPOINT_API_ROOT = 'https://{}-my.sharepoint.com/_api/v2.0/me/drive/root:/{}'

    def __init__(self, pwd, sharepoint, user_agent, cookie):
        super().__init__(self.SHAREPOINT_API_ROOT.format(sharepoint, pwd))
        self.headers = {
            "user-agent": user_agent,
            "cookie": cookie
        }

    def url(self, path):
        """Return relative file url
        
        Raises
        ------
        urllib.error.HTTPError
            If the user-agent and cookie is unauthorized or timeover
        """

        resp = json.load(urllib.request.urlopen(
                urllib.request.Request(self.pwd + '/' + quote(path),
                                       headers = self.headers)))

        return resp['@content.downloadUrl']
