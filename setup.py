#! /usr/bin/python3

from setuptools import setup, find_packages

setup(
    name = "basiccalibre",
    version = "0.0.1",
    keywords = ("calibre"),
    description = "Basic REST calibre api server",
    long_description = "Basic REST calibre api server based on calibre database",

    url = "http://c0ldcat.cc",
    author = "c0ldcat",
    author_email = "c0ldcat3z@gmail.com",

    packages = find_packages(),
    include_package_data = True,
    platforms = "any",
    install_requires = [
        "Flask",
        "Flask_RESTful" 
    ],

    scripts = [],
    entry_points = {
        'console_scripts': [
            'basiccalibre = basiccalibre:start'
        ]
    }
)
